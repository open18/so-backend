## Tech Stack

- NodeJS
- ExpressJS
- Mocha
- SuperTest

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Se inicia el servidor de dev

### `npm run test`

Se ejecutan las pruebas unitarias, para las pruebas unitarias se debe iniciar antes el servidor de desarrollo.

### `npm start`

Para inicia en modo produccion

