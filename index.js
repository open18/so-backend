const express = require("express");

console.log("starting...");
//creating the server
const app = express();

const cors = require("cors");

//enable the expess.json
app.use(express.json({ extended: true }));

//enable cors
app.use(cors());

//port the APP
const PORT = process.env.PORT || 4000;


//import routes
app.use('/api', require('./routes/public'));

app.listen(PORT, () => {
    console.log("The server is working in " + PORT);
});


