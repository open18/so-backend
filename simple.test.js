const request = require("supertest")("http://localhost:4000/api");
const expect = require("chai").expect;

describe("GET /iecho", function () {
    it("return text inverted and palindrome validation (sometemos)", async function () {
        const response = await request.get("/iecho?text=sometemos");
        expect(response.status).to.eql(200);
    });
    it("return bad params empty", async function () {
        const response = await request.get("/iecho");
        expect(response.status).to.eql(400);
    });
    it("return text inverted and palindrome validation (311564647)", async function () {
        const response = await request.get("/iecho?text=311564647");
        expect(response.status).to.eql(200);
    });
    it("return text inverted and palindrome validation (Edwin Acosta)", async function () {
        const response = await request.get("/iecho?text=Edwin Acosta");
        expect(response.status).to.eql(200);
    });
});