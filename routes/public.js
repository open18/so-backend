//routes for users
const express = require("express");
const router = express.Router();
const publicController = require("../controllers/publicController");

//- api/public/login GET
router.get('/iecho', publicController.iecho);

router.get('/ping', publicController.ping);

module.exports = router;