
exports.iecho = (req, res) => {
    const { text } = req.query;

    if (text)
        return res.json({ text: reverseString(text), palindrome: isPalindrome(text) });

    res.status(400);
    res.json({ text: "no text" });

}

exports.ping = (req, res) => {
    return res.json({ status: true, message: "Service is working..." });
}

const reverseString = text => {
    return text.split("").reverse().join("");
}

const isPalindrome = text => {
    const lowRegStr = text.toLowerCase();
    const reverseStr = lowRegStr.split('').reverse().join('');
    return reverseStr === lowRegStr;
}
